#!/bin/bash
source bin/activate

uniqueid=$1

systems=$(linode-cli linodes list|grep $uniqueid| awk '{print $2}')

echo $systems

for i in ${systems[@]}; do
    linode-cli linodes reboot $i
done;
