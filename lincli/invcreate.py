import sys,json,hcl

workerName = 'k3sworker'
masterName = 'k3scontroller'
workerGroup = 'workers'
masterGroup = 'controllers'

def readVarFile(varFile):
	"""Reads the Terraform TF vars file and returs a dict of contents
	varFile: String of the path to var file
	returns: dict of keyvalue variable"""

	cleaned = dict()
	try: 
		with open(varFile, "r") as vf: 
			vd = vf.readlines()
			for v in vd: 
				# ignors lines with a # or without an = to support commetened lines and igonor blank lines
				if  not v[0] == "#" and "=" in v :
					t = v.split("=")
					tc = t[1].strip('\n').strip("\"") # remote double quotes
					# print(f"{tc[2:]}")

					cleaned[t[0].strip(" ")] = tc[2:] # removes leading space and double quote
		# print(f"{ cleaned } ")
	except:
		raise
	return cleaned


def generateInventory(inventory_dict):
	"""Generates an ansible inventory for a k3s cluster form Linode-cli with --json option
	inventory_dict: dictionary systems with label and ip keys.
	returns: string representation of an Ansible inventory by group with host name and ip"""
	try:
		retval = ""
		for group in inventory_dict.keys():
			retval += f"\n[{group}]\n"	
			for groupsys in inventory_dict[group]:
				retval += f"{groupsys['label']} ansible_ssh_host={groupsys['ip']}\n"
		return retval

	except:
		raise

def readLinodes(linodesJson):
	"""Read Linode output from: linode-cli linodes list --json
	linodesJson: json file inventory file.
	returns: Dictionary of the inventory broken up into globally defined groups prefix names"""
	try:
		jdata = {}
		dict_inventory = {masterGroup:[], workerGroup:[]}

		lenworker = len(workerName)
		lenmaster = len(masterName)

		with open(linodesJson,"r") as jfile:
			jdata = json.load(jfile)

		for somesystem in jdata:
			if somesystem['label'][:lenworker] == workerName:
				dict_inventory[workerGroup].append({'label':somesystem['label'], 'ip':somesystem['ipv4'][0]}) 
			elif somesystem['label'][:lenmaster] == masterName:
				dict_inventory[masterGroup].append({'label':somesystem['label'], 'ip':somesystem['ipv4'][0]}) 
		return dict_inventory
	except:
		raise

def generateMasterUrl(inventory_dict):
	"""Read system names and set the master url to the IP address
	inventory_dict: dictionary systems with label and ip keys.
	returns: string for the master_url variable in an ansible inventory"""
	try:
		retval = "\n# Cluster Variable\n"
		master_ip = inventory_dict[masterGroup][0]['ip']
		retval += f"controller_url=https://{master_ip}:6443\n"
		return retval
	except IndexError as e:
		retval = "\n# Cluster Variable\n"
		retval += f"controller_url=https://controller1:6443\n"
		return retval
	except:
		raise

def readTerraformVariables(tfVarFile):
	"""Read Terraform Variables file
	tfVarFile: terraform formated file written to the use the variables names in the readme
	returns: dictionary output from the hcl library"""
	try:
		tfdata = {}
		with open(tfVarFile,'r') as tfFile:
			tfdata = hcl.loads(tfFile.read())
		return tfdata
	except:
		raise

def generateHostVariables(terraform_dict):
	"""Take the output from the readTerraformVariables function. Assuming the variables
	defined as specified in the readme, the login information for ansible is generated.
	terraform_dict: hcl output of the terraform variables file for Linode
	returns: string of ansible variables to be written to a file"""
	try:
		retval = "\n# Ansible Login information\n"
		retval += f"ansible_user={terraform_dict['variable']['kubernetes_user_name']['default']}\n"
		retval += f"ansible_ssh_pass={terraform_dict['variable']['kubernetes_user_password']['default']}\n"
		retval += f"ansible_become_pass={terraform_dict['variable']['kubernetes_user_password']['default']}\n"
		retval += f"ansible_become_method=sudo\n"
		return retval
	except:
		print(f"{json.dumps(terraform_dict,indent=4)}")
		raise

def generateHostVariablesTfvars(varFile):
	"""Pulls variabls from a tfvars file
	varFile: Path of vars file
	retuns: Ansible host formatted string"""
	try:
		terraVars = readVarFile(varFile)
		retval = "\n# Ansible Login information\n"
		retval += f"ansible_user={terraVars['kubernetes_user_name']}\n"
		retval += f"ansible_password={terraVars['kubernetes_user_password']}\n"
		retval += f"ansible_become_password={terraVars['kubernetes_user_password']}\n"
		retval += f"ansible_become_method=sudo\n"
		retval += f"ansible_connection=ssh\n"
		return retval
	except:
		print(f"{json.dumps(terraVars,indent=4)}")
		raise

# bad code, last minute SELF 2022 change
def monkeyNodeNames(varFile):
	"""Pulls variabls from a tfvars file, grabs unique_number, and edits node names globally
	varFile: Path of vars file
	retuns: void"""
	try:
		terraVars = readVarFile(varFile)
		global workerName
		global masterName
		workerName = f"k3s{terraVars['unique_number']}worker"
		masterName = f"k3s{terraVars['unique_number']}controller"
	except:
		pass # This is intentional so it will quietly fail when not needed. I'm not proud	

def main(args):
	outFileHosts = None  # outputs machine names 
	inLinodeJson = None  # json file pulled from the the linode cli.
	inTerraVars  = None  # terraform variables file for creating inventory  
	outFileLogin = None  # outputs login names from terraform variables 
	if len(args) < 2:
		raise ValueError("Need 2 or 3 arguments to use this fucntion")
	elif len(args) >= 2: 
		inLinodeJson = args[1]
	if len(args) >= 3:
		outFileHosts = args[2]
	if len(args) >= 4:
		inTerraVars = args[3]
	if len(args) >= 5:
		outFileLogin = args[4]

	try:
		monkeyNodeNames("../tf/terraform.tfvars")
		print(f"New names: {workerName}, {masterName}")
		dict_inventory = readLinodes(inLinodeJson)

		retHosts = generateInventory(dict_inventory)
		retval = retHosts

		if outFileHosts == None:
			return retval
		else:
			with open(outFileHosts,"w") as outfile:
				outfile.write(retHosts)
				outfile.close()
			# return retval

		retVars = generateMasterUrl(dict_inventory)
		dict_terraformVars = None
		if inTerraVars != None:
			try: # Attempt to read variables from variables.tf
				dict_terraformVars = readTerraformVariables(inTerraVars)
				retVars += generateHostVariables(dict_terraformVars)
			except KeyError: # Attempt to use tfvars instead. 
				try:
					retVars += generateHostVariablesTfvars("../tf/terraform.tfvars")
				except: 
					raise
			except:
				raise

			retval += retVars
			with open(outFileLogin,"w") as outfile:
				outfile.write(retVars)
				outfile.close()

		if outFileLogin == None or outFileHosts == None:
			return retval

	except:
		raise

if __name__ == "__main__":
	output = main(sys.argv)
	if output != None:
		print(output)
