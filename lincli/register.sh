#!/bin/bash
EXPIRE_DATE=$(date -d "+6 months" "+%Y-%m-%d")'T00:01:01.000Z'
linode-cli profile token-create \
  --scopes '*' \
  --expiry $EXPIRE_DATE \
  --label linode-cli
