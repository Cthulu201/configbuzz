#!/bin/bash
# A grabs the kubeconfig from the controller node and setups up the kubectl locally.
ansible controllers[0] -m fetch -a "src=/etc/rancher/k3s/k3s.yaml dest=~/.kube/config flat=yes" --become
CONTROLLER_URL=$(grep controller_url buzzcrate.hosts)
ansible localhost -m lineinfile -a "path=~/.kube/config regexp='    server: https://127.0.0.1:6443' line='    server: {{ controller_url }}' state=present" -e "$CONTROLLER_URL"