provider "linode" {
  token = var.linodeapikey
}

data "linode_user" "kubernetes" {
  username = var.linode_username
}

resource "linode_instance" "k3scontrollers" {
  label  = "k3s${var.unique_number}controller1"
  image  = "linode/arch"
  group  = "Terraform"
  region = "us-east"
  type   = "g6-standard-2"
  #  type             = "g7-highmem-1"
  private_ip       = true
  authorized_users = [data.linode_user.kubernetes.username]
  authorized_keys  = ["${chomp(file(var.ssh_key_pub))}"]
  root_pass        = var.archrootpass
  tags             = ["buzzcrate"]
  provisioner "remote-exec" {
    inline = [
      "hostnamectl set-hostname ${self.label}",
      "sed 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' -i /etc/sudoers",
      "useradd -m -g users -G wheel -s /bin/bash ${var.kubernetes_user_name}",
      "echo '${var.kubernetes_user_name}:${var.kubernetes_user_password}' | chpasswd",
      "ip addr add ${self.private_ip_address}/17 dev eth0",
      "pacman -Sy archlinux-keyring --noconfirm",
      "pacman -Syy",
      "pacman -Su --noconfirm",
      "pacman -S vim-minimal glibc wget screen curl base-devel open-iscsi jq --noconfirm",
    ]
    connection {
      type        = "ssh"
      user        = "root"
      private_key = chomp(file(var.ssh_key))
      # password = var.archrootpass
      host  = self.ip_address
      agent = false
    }
  }
}

resource "linode_instance" "k3sworkers" {
  count  = var.worker_count
  label  = "k3s${var.unique_number}worker${count.index}"
  image  = "linode/arch"
  group  = "Terraform"
  region = "us-east"
  type   = "g6-standard-2"
  #  type             = "g7-highmem-1"
  private_ip       = true
  authorized_users = [data.linode_user.kubernetes.username]
  authorized_keys  = ["${chomp(file(var.ssh_key_pub))}"]
  root_pass        = var.archrootpass
  tags             = ["buzzcrate"]
  provisioner "remote-exec" {
    inline = [
      "hostnamectl set-hostname ${self.label}",
      "sed 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' -i /etc/sudoers",
      "useradd -m -g users -G wheel -s /bin/bash ${var.kubernetes_user_name}",
      "echo '${var.kubernetes_user_name}:${var.kubernetes_user_password}' | chpasswd",
      "ip addr add ${self.private_ip_address}/17 dev eth0",
      "pacman -Sy archlinux-keyring --noconfirm",
      "pacman -Syy",
      "pacman -Su --noconfirm",
      "pacman -S vim-minimal glibc wget screen curl base-devel open-iscsi jq --noconfirm",
    ]
    connection {
      type        = "ssh"
      user        = "root"
      private_key = chomp(file(var.ssh_key))
      # password = var.archrootpass
      host  = self.ip_address
      agent = false
    }
  }
}
