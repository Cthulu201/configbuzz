# Linode Arch 

[[_TOC_]]

Presently testing to make an Arch installation for Linode via Terraform.

## Setup

1. Have Terraform installed
1. Set up variables file. 

### Variables file 

Name of file: `variables.tf`

Example Contents of `terraform.tfvars`:
```terraform
linodeapikey             = "yourUsernameApiKey"
unique_number            = "1122"
archrootpass             = "SUp3rS3C8rePassw0rd"
ssh_key_pub              = "~/.ssh/linode.pub"
ssh_key                  = "~/.ssh/linode"
linode_username          = "yourLinodeUsername"
kubernetes_user_name     = "kuber"
kubernetes_user_password = "kuberbuzzcratepassword"
worker_count             = 5
```
**NOTE:** Please generate your own ssh key and match it with the example above. 

## Example use 

1. `terraform init`
1. `terraform plan`
1. `terraform apply`
1. `terraform destroy`

# References

* [Linode CCM Unmanged K8s](https://www.linode.com/docs/guides/installing-the-linode-ccm-on-an-unmanaged-kubernetes-cluster/)


# Generated Module Variables
## Module Data
| Data Name |
| :--- | 
| linode_user |

## Module Resources
| Resource Name |
| :--- | 
| linode_instance |

## Module Variables
| Variable Name | Variable Description | Type | Default |
| :--- | :--- | :---: | ---: |
| linodeapikey | Linode ApiKey | ${string} | None |
| unique_number | Unique number so cluster don't impact eachother | ${string} | 00 |
| archrootpass | Arch Linux Root Password | ${string} | None |
| ssh_key_pub | SSH Public Key | ${string} | None |
| ssh_key | SSH Private Key | ${string} | None |
| linode_username | Username to be used as noted in the Linode profile | ${string} | None |
| kubernetes_user_name | Username used for the kubernetes user | ${string} | None |
| kubernetes_user_password | Password for the kubernetes user | ${string} | None |
| worker_count | Number of workers | ${number} | None |

